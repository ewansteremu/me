#themaddog-description 
** Une liste organisée d'environnements de piratage où vous pouvez former vos compétences cybernétiques légalement et en toute sécurité **

Pour tout le monde dans le domaine de la sécurité de l'information, il est important de comprendre l'ennemi, le pirate informatique. Comprendre l'ennemi fait de vous le meilleur défenseur possible pour sécuriser le monde numérique.

    EN CONNAISSANT VOTRE ENNEMI, VOUS POUVEZ VAINCRE VOTRE ENNEMI.

Il possède une vaste expérience dans l'informatique, l'édition de logiciel et la sécurité de l'information en tant que passionné, formateur, consultant et chercheur international, avec une expérience réussie dans le développement et la direction de programmes techniques de cybersécurité d'entreprise pour les organisations militaires et mondiales.

Il utilise la gestion des risques dans la phase de planification tout en mettant en œuvre les mesures de défense appropriées au bon endroit et a des équipes dirigeantes pour le faire. Il a réalisé des centaines d'évaluations et d'audits de sécurité pour de nombreux clients.

Outre les évaluations de vulnérabilité, il donne des cours avancés de piratage EN DIRECT, il prendra en charge un serveur / ordinateur, un téléphone ou pirater tout le monde dans la pièce en un seul coup, sans blesser personne.

En utilisant les dernières techniques de piratage, il éduquera également le public pour expliquer ce qui vient de se passer. Il utilise les attaques Man-in-the-Middle, l'empoisonnement ARP, le reniflage, les injections de script, l'usurpation DNS et les sessions de détournement qui se produisent sur les appareils et explique comment les empêcher dans un cadre LIVE.

Il peut également simuler une attaque DOS qui fait tomber les réseaux les plus puissants.

Bien connu des services militaires français sous le nom de "The MadDoG", il a présenté pour le magazine képi blanc le piratage informatique et la cybercriminalité.

Il a commencé sa carrière en sécurité de l'information à la Légion Etrangère où il a effectué des recherches sur la cybersécurité, la cybercriminalité, la cyber-criminalistique, la confidentialité et la protection des données.

Il possède une solide expertise dans le développement d'architectures de concept et de cybersécurité avec une grande expérience technique pratique dans les systèmes de cybersécurité. Il était responsable d'importants travaux de recherche via OWASP, OSINT & Darknet et il était responsable de nombreux projets d'assurance de l'information qui impliquaient des approches et des systèmes de cybersécurité pour la détection, la prévention et l'atténuation des activités malveillantes.

Il a également parlé fréquemment de la cybersécurité et du piratage lors de conférences militaires et professionnelles et a publié des articles et des blogs sur des questions liées à la cybersécurité.
